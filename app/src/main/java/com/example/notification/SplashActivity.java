package com.example.notification;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import javax.net.ssl.KeyManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        try {
            if (KeyManager.getSharedPreferenceBoolean(SplashActivity.this, "isLoggedIn", false)) {
                if (getIntent().hasExtra("pushnotification")) {
                    Intent intent = new Intent(this, YourDesiredActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    CheckLogin();
                }
            } else {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }

        } catch (Exception e) {
            CheckLogin();
            e.printStackTrace();
        }
    }
}
